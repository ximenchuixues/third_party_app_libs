<h1 align="center">OpenHarmony三方组件开发者通讯录</h1>


### 本库收集了OpenHarmony三方组件中开发者的通讯录，对OpenHarmony三方组件感兴趣的开发者可通过编辑本文档自发填写自己的通讯方式并提交Pull Requests，以便获取OpenHarmony的最新动态。

#### 以下表格带‘*’条目为必填选项


|昵称*|邮箱*|gitee账号*|姓名|手机号|QQ号码|微信号码|
|:---:|:---:|:---:|---:|:---:|:---:|:---:|
| Catherine | 326510838@qq.com | 326510838@qq.com | 柯映昌 | 15762264023 | 326510838 |  keyingchang |
| dodozhou7 | abczp777777@163.com | abczp777777@163.com | 周黎生 | 18758878167 | 635547767 |  zhoulisheng |